﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UAI.TD._2018.PrimeraClase.Interfaces;

namespace UAI.TD._2018.PrimeraClase.Validators
{
  
    public static class ValidationExtensions
    {
        public static bool Validate<T>(this T entity) where T : IBaseEntity
        {

            var validator = ValidatorFactory.GetValidator<T>();

            var nvc = validator.Validate(entity);

            if (nvc.Count > 0)
                throw new BussinessValidationException(nvc);

            return true;

        }
    }
}
