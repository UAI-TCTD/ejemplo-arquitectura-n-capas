﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UAI.TD._2018.PrimeraClase.Entities;
using UAI.TD._2018.PrimeraClase.Impl.Entities;

namespace UAI.TD._2018.PrimeraClase.Validators
{
    public class UsuarioValidator : AbstractValidator<Usuario>
    {
        public override NameValueCollection Validate(Usuario entity)
        {
            
            if (string.IsNullOrEmpty(entity.Nombre))
                _nvc.Add("E01", "El nombre es obligatorio");


            return _nvc;
            
        }
    }
}
