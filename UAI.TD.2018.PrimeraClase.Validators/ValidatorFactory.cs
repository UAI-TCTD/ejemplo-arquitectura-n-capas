﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using UAI.TD._2018.PrimeraClase.Interfaces;

namespace UAI.TD._2018.PrimeraClase.Validators
{
    public class ValidatorFactory
    {
        public static IValidator<T> GetValidator<T>() where T : IBaseEntity
        {

            var t = typeof(T);


            try
            {

                var ns = typeof(ValidatorFactory).Namespace;


            
                //esto es reflection
                IValidator<T> validator = (IValidator<T>)Assembly.LoadFrom("UAI.TD.2018.PrimeraClase.Impl.Validators.dll").CreateInstance(String.Format("{0}.{1}Validator", ns, t.Name));



                if (validator == null) return new MockValidator<T>();
                return validator;

            }
            catch (Exception e)
            {
                
                return new MockValidator<T>();
            }




        }
    }
}
