﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UAI.TD._2018.PrimeraClase.Interfaces;

namespace UAI.TD._2018.PrimeraClase.Validators
{
    public abstract class AbstractValidator<T> : IValidator<T> where T : IBaseEntity
    {
        protected NameValueCollection _nvc = new NameValueCollection();

        public abstract NameValueCollection Validate(T entity);

    }

}
