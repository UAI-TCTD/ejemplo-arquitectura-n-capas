﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UAI.TD._2018.PrimeraClase.Validators
{
    public class BussinessValidationException : Exception
    {

        NameValueCollection _nvc;

        public BussinessValidationException(string key, string error)
        {
            _nvc = new NameValueCollection();
            _nvc.Add(key, error);
        }
        public BussinessValidationException(NameValueCollection nvc)
        {
            _nvc = nvc;
        }

        public override string Message
        {


            get
            {
                return string.Join(",", _nvc.AllKeys.ToDictionary(x => x, x => _nvc[x]));

            }
        }

    }
}
