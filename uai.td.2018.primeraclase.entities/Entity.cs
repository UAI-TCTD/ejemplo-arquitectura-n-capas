﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UAI.TD._2018.PrimeraClase.Interfaces;

namespace UAI.TD._2018.PrimeraClase.Entities
{
    public abstract class Entity : IBaseEntity
    {
        public string Id { get ; set ; }

        public bool IsTransient()
        {
            return Id==null || Id.Equals(Guid.Empty.ToString());
        }
    }
}
