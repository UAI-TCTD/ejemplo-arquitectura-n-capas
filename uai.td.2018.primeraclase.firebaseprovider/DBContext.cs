﻿
using Firebase.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UAI.TD._2018.PrimeraClase.Interfaces;
using Firebase.Database.Query;

namespace UAI.TD._2018.PrimeraClase.FirebaseProvider
{
    public class DBContext<T> : ICrud<T> where T : IBaseEntity
    {
        FirebaseClient _client;
        string _child;
        public DBContext()
        {
            _child = typeof(T).Name;


            var url = "https://td-2018-uai.firebaseio.com/";


            _client = new FirebaseClient(url);



        }
        public void Delete(T entity)
        {
            _client.Child(_child).DeleteAsync();
        }

        public IList<T> GetAll()
        {

            var res = _client.Child(_child).OnceAsync<T>();

            var yx =  res.Result.ToList().Select(y => y.Object).ToList();

            return yx;
        }

        public T Save(T entity)
        {
            entity.Id = Guid.NewGuid().ToString();



            var xx =  _client.Child(_child).PostAsync<T>(entity);

            var xy = xx.Result.Key;

            return xx.Result.Object;

        }

        public T Update(T entity)
        {
            throw new NotImplementedException();
        }
    }
}
