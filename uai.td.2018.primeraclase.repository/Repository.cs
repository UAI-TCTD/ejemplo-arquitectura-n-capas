﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UAI.TD._2018.PrimeraClase.Interfaces;

namespace UAI.TD._2018.PrimeraClase.Repository
{

    public abstract class Repository<T> : ICrud<T> where T : IBaseEntity
    {
        protected ICrud<T> _context;
        public Repository(ICrud<T> context)
        {
            _context = context;
        }
        public abstract void Delete(T entity);
        public abstract IList<T> GetAll();
        public abstract T Save(T entity);
        public abstract T Update(T entity);
       
    }
}
