﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UAI.TD._2018.PrimeraClase.FirebaseProvider;
using UAI.TD._2018.PrimeraClase.Impl.Entities;
using UAI.TD._2018.PrimeraClase.Repository;

namespace UAI.TD._2018.PrimeraClase.Impl.Repository
{
    public class UsuarioRepository : Repository<Usuario>
    {

        public UsuarioRepository() : base(new UsuarioContext())
        {
       
        }
        public override void Delete(Usuario entity)
        {
            _context.Delete(entity);
        }

        public override IList<Usuario> GetAll()
        {
            return _context.GetAll();
        }

        public override Usuario Save(Usuario entity)
        {
            return _context.Save(entity);
        }

        public override Usuario Update(Usuario entity)
        {
            return _context.Update(entity);
        }
    }
}
