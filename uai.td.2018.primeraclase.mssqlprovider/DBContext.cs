﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UAI.TD._2018.PrimeraClase.Interfaces;

namespace UAI.TD._2018.PrimeraClase.MsSqlProvider
{
    public abstract class DBContext<T> : ICrud<T> where T : IBaseEntity
    {
        protected IUnitOfWork _uow;
        public abstract void Delete(T entity);
        public abstract IList<T> GetAll();
        public abstract T Save(T entity);
        public abstract T Update(T entity);
       
    }
}
