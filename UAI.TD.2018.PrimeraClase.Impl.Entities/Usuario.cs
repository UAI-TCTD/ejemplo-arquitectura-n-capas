﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UAI.TD._2018.PrimeraClase.Entities;

namespace UAI.TD._2018.PrimeraClase.Impl.Entities
{
    public class Usuario : Entity
    {
        public string Nombre { get; set; }
        public string Clave { get; set; }
    }
}
