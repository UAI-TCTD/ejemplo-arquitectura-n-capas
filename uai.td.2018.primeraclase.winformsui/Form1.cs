﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UAI.TD._2018.PrimeraClase.Application;
using UAI.TD._2018.PrimeraClase.Entities;
using UAI.TD._2018.PrimeraClase.Impl.Application;
using UAI.TD._2018.PrimeraClase.Impl.Entities;
using UAI.TD._2018.PrimeraClase.Validators;

namespace UAI.TD._2018.PrimeraClase.WinformsUI
{
    public partial class Form1 : Form
    {
        UsuarioApplication _usuarios;

        Usuario usuario;
        public Form1()
        {
            InitializeComponent();
            _usuarios = new UsuarioApplication();
            RetrieveData();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            usuario = new Usuario();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                if (usuario == null) usuario = new Usuario();

                usuario.Nombre = this.txtNombre.Text;
                usuario.Clave = this.txtClave.Text;
                if (usuario.IsTransient())
                    usuario = _usuarios.Save(usuario);
                else
                    usuario = _usuarios.Update(usuario);

                RetrieveData();
            }
            catch (BussinessValidationException xe)
            {

                MessageBox.Show(xe.Message);
            }
          

        }


        private void RetrieveData()
        {
            this.dataGridView1.DataSource = null;
            this.dataGridView1.DataSource = _usuarios.GetAll();
        }
    }
}
