﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UAI.TD._2018.PrimeraClase.Interfaces
{
    public interface IBaseEntity
    {
        string Id { get; set; }
        bool IsTransient();
    }
}
