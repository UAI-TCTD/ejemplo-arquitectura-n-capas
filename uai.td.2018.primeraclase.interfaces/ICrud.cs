﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UAI.TD._2018.PrimeraClase.Interfaces
{
    public interface ICrud<T> where T : IBaseEntity
    {
        T Save(T entity);
        void Delete(T entity);
        IList<T> GetAll();
        T Update(T entity);

    }
}
