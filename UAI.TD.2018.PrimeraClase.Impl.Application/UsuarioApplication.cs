﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UAI.TD._2018.PrimeraClase.Application;

using UAI.TD._2018.PrimeraClase.Impl.Entities;
using UAI.TD._2018.PrimeraClase.Impl.Repository;

namespace UAI.TD._2018.PrimeraClase.Impl.Application
{
    public class UsuarioApplication : Application<Usuario>
    {
        public UsuarioApplication() : base(new UsuarioRepository())
        {

        }
    }
}
